import React, { useState, useEffect } from 'react';
import { View, Text, Image, Linking ,Button} from 'react-native';
import { WebView } from 'react-native-webview';
import Auth0 from 'react-native-auth0';

const AUTH0_DOMAIN = 'dev-05h776n0.auth0.com';
const AUTH0_CLIENT_ID = 'KStfnDtq49muSbVsBbcSwIi9fEdwJPan';
const AUTH0_REDIRECT_URI = 'authtest://dev-05h776n0.auth0.com/ios/com.anonymous.authtest/callback';

const auth0 = new Auth0({ domain: AUTH0_DOMAIN, clientId: AUTH0_CLIENT_ID, sso: false, ephemeralSession: true });

const Auth0WebView = () => {
  const [authError, setAuthError] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  const [authUrl, setAuthUrl] = useState(null);

  useEffect(() => {
    const url = auth0.auth.authorizeUrl({
      responseType: 'token',
      scope: 'openid profile email',
      redirectUri: AUTH0_REDIRECT_URI,
      state: "fares"
    });
    setAuthUrl(url);

    const handleDeepLink = event => {
      const url = event.url;
      console.log('Deep link received: ', url);
      processUrl(url);
    };

  const deepLinkSubscription = Linking.addEventListener('url', handleDeepLink);

  Linking.getInitialURL().then(url => {
    if (url) handleDeepLink({ url });
  });

    return () => {
      deepLinkSubscription.remove();

    };
  }, []);

  const processUrl = (url) => {
    if (url.includes(AUTH0_REDIRECT_URI)) {
      const accessTokenMatch = url.match(/access_token=([^&]+)/);
      if (accessTokenMatch) {
        const accessToken = accessTokenMatch[1];
        console.log('Access Token:', accessToken);
        fetchUserInfo(accessToken);
      } else {
        console.error('Access token not found in the deep link URL');
      }
    }
  };

  const fetchUserInfo = (accessToken) => {
    auth0.auth
      .userInfo({ token: accessToken })
      .then(user => {
        setUserInfo(user);
      })
      .catch(error => {
        console.log(error);
        setAuthError('Failed to fetch user info');
      });
  };

  const handleNavigationStateChange = (navState) => {
    console.log(navState.url);
    processUrl(navState.url);
  };
 const logout = () => {
  auth0.webAuth
    .clearSession({})
    .then(() => {
      setUserInfo(null);
      setAuthError(null); 
    })
    .catch(error => console.log('Logout Error:', error));
};
  return (
    <View style={{ flex: 1 }}>
      {userInfo ? (
        <View style={{margin:20,justifyContent:'center',alignItems:'center'}}>
          <Text>Name : {userInfo.name}</Text>
          <Image source={{ uri: userInfo.picture }} style={{ width: 100, height: 100 }} />
          <Button title="Logout" onPress={logout} />
        </View>
      ) : (
        <WebView
          source={{ uri: authUrl }}
          onNavigationStateChange={handleNavigationStateChange}
          sharedCookiesEnabled={true}
          incognito={false}
          style={{ flex: 1 }}
          startInLoadingState={true}

        />
      )}
    </View>
  );
};

export default Auth0WebView;
