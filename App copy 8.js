import React, { useState,useEffect } from 'react';
import { WebView } from 'react-native-webview';
import {Linking} from 'react-native'

const AUTH0_DOMAIN = 'dev-05h776n0.auth0.com';
const AUTH0_CLIENT_ID = 'KStfnDtq49muSbVsBbcSwIi9fEdwJPan';
const AUTH0_REDIRECT_URI = 'authtest://dev-05h776n0.auth0.com/ios/com.anonymous.authtest/callback';
import {useAuth0, Auth0Provider} from 'react-native-auth0';


const Auth0WebView = () => {
  const [authError, setAuthError] = useState(null);
  useEffect(() => {
    const handleDeepLink = (event) => {
      const url = event.url;
      console.log('Deep link received: ', url);

      // Check if the URL is from your Auth0 redirect
      if (url.includes('authtest://dev-05h776n0.auth0.com/ios/com.anonymous.authtest/callback')) {
        // Parse the URL and extract the access token
        let accessToken = null;
        const matches = url.match(/access_token=([^&]+)/);
        if (matches && matches.length > 1) {
          accessToken = matches[1];
          console.log('Access Token:', accessToken);

          // TODO: Navigate to your desired screen or perform other actions
        } else {
          // Handle the error or lack of token in the URL
          console.error('Access token not found in the deep link URL');
        }
      }
    };



    
    // Get the initial URL that opened the app
    Linking.getInitialURL().then((url) => {
      if (url) {
        handleDeepLink({ url });
      }
    });

    // Add event listener for new incoming URLs
    Linking.addEventListener('url', handleDeepLink);

    // Cleanup the event listener
    return () => {
      Linking.removeEventListener('url', handleDeepLink);
    };
  }, []);

  const authUrl = `https://${AUTH0_DOMAIN}/authorize` +
    `?response_type=token` +
    `&client_id=${encodeURIComponent(AUTH0_CLIENT_ID)}` +
    `&redirect_uri=${encodeURIComponent(AUTH0_REDIRECT_URI)}` +
    `&scope=openid%20profile%20email`;

  const handleNavigationStateChange = (navState) => {
    console.log(navState.url)
    if (navState.url.startsWith(AUTH0_REDIRECT_URI)) {
      // Check for error in URL
      const errorMatch = navState.url.match(/[&?]error=([^&]+)/);
      if (errorMatch) {
        setAuthError(errorMatch[1]);
        return;
      }

      // Extract access token
      const accessTokenMatch = navState.url.match(/access_token=([^&]+)/);
      if (accessTokenMatch) {
        const accessToken = accessTokenMatch[1];
        console.log('Access Token:', accessToken);
        // Use the access token as needed in your app
      } else {
        setAuthError('Access token not found in the callback URL.');
      }
    }
  };

  return (
    <WebView 
      source={{ uri: authUrl }}
      onNavigationStateChange={handleNavigationStateChange}
      sharedCookiesEnabled={true} // Enable shared cookies on iOS
      incognito={false} // Ensure session continuity
      style={{flex:1}}
    />
  );
};

export default Auth0WebView;
