import { NativeModules } from 'react-native';

const { AuthModule } = NativeModules;

export default {
  showAuthController(url) {
    return new Promise((resolve, reject) => {
      AuthModule.showAuthController(url, resolve, reject);
    });
  },
};
