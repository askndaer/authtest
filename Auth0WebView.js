import React, { useState } from 'react';
import { WebView } from 'react-native-webview';
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_REDIRECT_URI ,AUTH_CLIENT_SECRET} from './auth0-config';

const Auth0WebView = ({ onTokenReceived }) => {
  const [url, setUrl] = useState(`https://${AUTH0_DOMAIN}/authorize?` +
    `response_type=code&` +
    `client_id=${AUTH0_CLIENT_ID}&` +
    `redirect_uri=${AUTH0_REDIRECT_URI}&` +
    `scope=openid profile email`);

  const handleNavigationStateChange = async (newNavState) => {
    const { url } = newNavState;
    if (!url) return;
console.log("url:",url)
    // Check if we are redirected to the redirect_uri
    if (url.startsWith(`https://${AUTH0_DOMAIN}/authorize/resume?state=`)) {
 
    }
  };
  
  

  return (
    <WebView
      source={{ uri: url }}
      onNavigationStateChange={handleNavigationStateChange}
    />
  );
};

export default Auth0WebView;
