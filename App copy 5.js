import React, { useState } from 'react';
import { View, Button, Text, Image, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';
import Auth0 from 'react-native-auth0';
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_REDIRECT_URI } from './auth0-config';


const auth0 = new Auth0({ domain: AUTH0_DOMAIN, clientId: AUTH0_CLIENT_ID,sso: false, });

const App = () => {
  const [accessToken, setAccessToken] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  const [showWebView, setShowWebView] = useState(false);

  const login = () => {
    setShowWebView(true);
  };

  
  function generateRandomString(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  const logout = () => {
    auth0.webAuth
      .clearSession({})
      .then(() => {
        setAccessToken(null);
        setUserInfo(null);
      })
      .catch(error => console.log(error));
  };

  const fetchUserInfo = (accessToken) => {
    auth0.auth
      .userInfo({ token: accessToken })
      .then(user => {
        setUserInfo(user);
        setShowWebView(false);
      })
      .catch(error => console.log(error));
  };

  const customUserAgent = 'Mozilla/5.0 (platform; rv:geckoversion) Gecko/geckotrail MyCustomApp/1.0.0';



  const renderWebView = () => {
    const randomString = generateRandomString(32);
    // const authUrl = `https://${AUTH0_DOMAIN}/authorize?client_id=${AUTH0_CLIENT_ID}&response_type=token&scope=openid profile email&redirect_uri=${AUTH0_REDIRECT_URI}&state=${randomString}`;

    const authUrl = auth0.auth.authorizeUrl({
      responseType: 'code',
      scope: 'openid profile email',
      redirectUri: AUTH0_REDIRECT_URI,
      state: randomString
    });
    console.log("authURL: ", authUrl)
    
    return (
      <WebView
        source={{ uri: authUrl }}
       
        javaScriptEnabled={true}
        domStorageEnabled={true}
        decelerationRate="normal"
        startInLoadingState={true}
        sharedCookiesEnabled={true}
        userAgent={customUserAgent}

        // automaticallyAdjustContentInsets={false}
        onNavigationStateChange={event => {
          console.log('Navigated to:', event.url);

          // Check if the URL is the redirect URI
          if (event.url.startsWith(AUTH0_REDIRECT_URI)) {
            const url = new URL(event.url);
            const accessToken = url.searchParams.get('access_token');
            const state = url.searchParams.get('state');
            const storedState = ''/* Retrieve your stored state value here */;

            if (state !== storedState) {
              console.error('State value does not match');
              setShowWebView(false);
              return;
            }

            if (accessToken) {
              setAccessToken(accessToken);
              fetchUserInfo(accessToken);
              setShowWebView(false);
            } else {
              console.error('Access token not found');
              setShowWebView(false);
            }
          }
        }}
        style={styles.webView}
      />
    );
  };

  return (
    <View style={styles.container}>
      {showWebView ? (
        renderWebView()
      ) : (
        <View style={styles.content}>
          {!accessToken ? (
            <Button title="Login" onPress={login} />
          ) : (
            <View>
              <Text>Logged in!</Text>
              {userInfo && (
                <View>
                  <Text>Name: {userInfo.name}</Text>
                  <Image
                    source={{ uri: userInfo.picture }}
                    style={styles.image}
                  />
                </View>
              )}
              <Button title="Logout" onPress={logout} />
            </View>
          )}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  webView: {
    marginTop: 20,
    maxHeight: 400,

    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
  },
});

export default App;
