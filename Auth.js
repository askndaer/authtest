// Auth.js
import { AuthSession } from 'expo';
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_REDIRECT_URI } from './auth0-config';

export async function loginWithEmailPassword(email, password) {
  const redirectUrl = AuthSession.getRedirectUrl();
  const responseType = 'code';
  const scope = 'openid profile email';
  const prompt = 'login';

  const authUrl =
    `https://${AUTH0_DOMAIN}/authorize` +
    `?client_id=${AUTH0_CLIENT_ID}` +
    `&redirect_uri=${encodeURIComponent(redirectUrl)}` +
    `&response_type=${responseType}` +
    `&scope=${encodeURIComponent(scope)}` +
    `&prompt=${prompt}`;

  const tokenUrl = `https://${AUTH0_DOMAIN}/oauth/token`;

  const formData = new FormData();
  formData.append('grant_type', 'password');
  formData.append('username', email);
  formData.append('password', password);
  formData.append('scope', scope);
  formData.append('client_id', AUTH0_CLIENT_ID);

  const tokenResponse = await fetch(tokenUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: formData,
  });

  const tokenResult = await tokenResponse.json();
  console.log('Token result:', tokenResult);

  return tokenResult;
}

export async function logout() {
  // Implement logout logic if needed
}
