import React, { useState } from 'react';
import { View, Button, Text, Image } from 'react-native';
import Auth0 from 'react-native-auth0';

import { AUTH0_DOMAIN, AUTH0_CLIENT_ID } from './auth0-config';

const auth0 = new Auth0({ domain: AUTH0_DOMAIN, clientId: AUTH0_CLIENT_ID });

const App = () => {
  const [accessToken, setAccessToken] = useState(null);
  const [userInfo, setUserInfo] = useState(null);

  const login = () => {
    auth0.webAuth
      .authorize({ scope: 'openid profile email' })
      .then(credentials => {
        setAccessToken(credentials.accessToken);
        fetchUserInfo(credentials.accessToken);
      })
      .catch(error => console.log(error));
  };

  const fetchUserInfo = (accessToken) => {
    auth0.auth
      .userInfo({ token: accessToken })
      .then(user => {
        setUserInfo(user);
      })
      .catch(error => console.log(error));
  };

  const logout = () => {
    auth0.webAuth
      .clearSession({})
      .then(success => {
        setAccessToken(null);
        setUserInfo(null);
      })
      .catch(error => console.log(error));
  };

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      {!accessToken ? (
        <Button title="Login" onPress={login} />
      ) : (
        <View>
          <Text>Logged in!</Text>
          {userInfo && (
            <View>
            <Text>Name: {JSON.stringify(userInfo)}</Text>
              <Text>Name: {userInfo.name}</Text>
              <Image
                source={{ uri: userInfo.picture }}
                style={{ width: 100, height: 100 }}
              />
            </View>
          )}
          <Button title="Logout" onPress={logout} />
        </View>
      )}
    </View>
  );
};

export default App;
