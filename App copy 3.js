import React, { useState, useEffect } from 'react';
import { View, Text, Button } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Auth0WebView from './Auth0WebView';
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_REDIRECT_URI } from './auth0-config';

const App = () => {
  const [accessToken, setAccessToken] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  const [showWebView, setShowWebView] = useState(false);

  useEffect(() => {
    AsyncStorage.getItem('accessToken').then(token => {
      if (token) {
        setAccessToken(token);
        fetchUserInfo(token);
      }
    });
  }, []);

  const handleTokenReceived = async (token) => {
    await AsyncStorage.setItem('accessToken', token);
    setAccessToken(token);
    fetchUserInfo(token);
    setShowWebView(false);
  };

  const fetchUserInfo = async (token) => {
    const response = await fetch(`https://${AUTH0_DOMAIN}/userinfo`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (response.ok) {
      const data = await response.json();
      setUserInfo(data);
    } else {
      // Handle error
      console.error('Failed to fetch user info');
    }
  };

  const handleLogout = async () => {
    await AsyncStorage.removeItem('accessToken');
    setAccessToken(null);
    setUserInfo(null);
    // Add logout logic for Auth0
  };

  return (
    <View style={{ flex: 1, margin: 20 }}>
      {!accessToken ? (
        showWebView ? (
          <Auth0WebView onTokenReceived={handleTokenReceived} />
        ) : (
          <Button title="Login" onPress={() => setShowWebView(true)} />
        )
      ) : (
        <View>
          {userInfo ? (
            <Text>Welcome, {userInfo?.name}!</Text>
          ) : (
            <Text>Loading user info...</Text>
          )}
          <Button title="Logout" onPress={handleLogout} />
        </View>
      )}
    </View>
  );
};

export default App;
