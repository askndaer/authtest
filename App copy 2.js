// App.js
import React, { useState } from 'react';
import { View, StyleSheet ,Text} from 'react-native';
import WebView from 'react-native-webview';
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_REDIRECT_URI } from './auth0-config';
import AsyncStorage from '@react-native-async-storage/async-storage';


const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const storeData = async (key,value) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      // saving error
    }
  };
  const auth0Url = `https://${AUTH0_DOMAIN}/authorize?` +
    `client_id=${AUTH0_CLIENT_ID}&` +
    `scope=openid%20profile%20email&` +
    `redirect_uri=${encodeURIComponent(AUTH0_REDIRECT_URI)}&` +
    `response_type=code`;

  const onNavigationStateChange = (navState) => {
    console.log("url: ",navState.url)
    if (navState.url.startsWith(`https://${AUTH0_DOMAIN}/authorize/resume?state=`)) {
      const urlParams = new URLSearchParams(url.split('?')[1]);
        const state = urlParams.get('state');
        storeData('@state',state)
      setIsLoggedIn(true);
    }
    // if (navState.url.startsWith(AUTH0_REDIRECT_URI)) {
    //   // Handle the callback here, extract the code, and exchange it for tokens
    //   setIsLoggedIn(true);
    // }
  };

  return (
    <View style={styles.container}>
      <Text>{`Is Logged In: ${isLoggedIn}`}</Text>
      {!isLoggedIn ? (
        <WebView
          source={{ uri: auth0Url }}
          onNavigationStateChange={onNavigationStateChange}
          style={styles.webview}
        />
      ) : (
        <View style={styles.loggedInContainer}>
          {/* Display logged in state */}
          <Text>User is logged in.</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20, // Adjust margin or padding as needed
  },
  webview: {
    flex: 1, // Ensure WebView takes full available space
  },
  loggedInContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
