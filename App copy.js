import React, { useEffect,useState } from 'react';
import { StyleSheet,  View, Button, Linking,Modal,Text,TouchableOpacity } from 'react-native';
import { Auth0Provider, useAuth0 } from 'react-native-auth0';
import LoginScreen from './loginScreen'


export default function App() {
  const [isModalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    const handleDeepLink = (event) => {
      const url = event.url;
      const parsedUrl = new URL(url);
      const path = parsedUrl.pathname;
      const queryParams = Object.fromEntries(new URLSearchParams(parsedUrl.search));

      if(queryParams.param1 === 'value3'){
        setModalVisible(true);
      }

      console.log(`Path: ${path}, Data: ${JSON.stringify(queryParams)}`);
    };

    const deepLinkSubscription = Linking.addEventListener('url', handleDeepLink);

    Linking.getInitialURL().then((url) => {
      if (url) handleDeepLink({ url });
    });

    return () => {
      // deepLinkSubscription.remove();
    };
  }, []);

  const closeModal = () => {
    setModalVisible(false);
  };

  return (
    <View style={{flex:1}}>
    
      <Auth0Provider domain="dev-05h776n0.auth0.com" clientId="LCvbKA6Y9WFsnaG66BOmosqstDzKKH4o">
        <View style={styles.container}>
          <LoginScreen/>
        </View>
      </Auth0Provider>
      <Modal
            transparent={true}
            visible={isModalVisible}
            onRequestClose={closeModal}
          >
            <View style={styles.modalView}>
              <Text>Your custom message or content here</Text>
              <TouchableOpacity onPress={closeModal}>
                <Text>Close</Text>
              </TouchableOpacity>
            </View>
          </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    margin: 20,
    backgroundColor: '#00d3d0',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
